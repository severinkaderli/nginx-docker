#!/bin/sh

[ -z "${PHP_UPSTREAM}" ] && export PHP_UPSTREAM=0
[ -z "${ROOT_DIR}" ] && export ROOT_DIR=/usr/share/nginx/html
envsubst '${PHP_UPSTREAM},${ROOT_DIR}' < /etc/nginx/nginx.conf.template > /etc/nginx/nginx.conf
nginx -g "daemon off;"