FROM nginx:alpine

COPY nginx.conf.template /etc/nginx/nginx.conf.template
RUN rm /usr/share/nginx/html/index.html
ADD start.sh /usr/local/bin/start.sh
RUN chmod 777 /usr/local/bin/start.sh
CMD /usr/local/bin/start.sh